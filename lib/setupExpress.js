'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = setupExpress;

var _auth0ExtensionTools = require('auth0-extension-tools');

var _auth0ExtensionTools2 = _interopRequireDefault(_auth0ExtensionTools);

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _morgan = require('morgan');

var _morgan2 = _interopRequireDefault(_morgan);

var _expressSession = require('express-session');

var _expressSession2 = _interopRequireDefault(_expressSession);

var _cookieParser = require('cookie-parser');

var _cookieParser2 = _interopRequireDefault(_cookieParser);

var _bodyParser = require('body-parser');

var _bodyParser2 = _interopRequireDefault(_bodyParser);

var _connectFlash = require('connect-flash');

var _connectFlash2 = _interopRequireDefault(_connectFlash);

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _wsfed = require('./routes/wsfed');

var _wsfed2 = _interopRequireDefault(_wsfed);

var _wsfedAsync = require('./wsfedAsync');

var _wsfedAsync2 = _interopRequireDefault(_wsfedAsync);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function setupExpress(config, storage, getExtensibility) {
    var app = (0, _express2.default)();

    // Allows this to sit-behind webtask and ngrok
    if (config('NODE_ENV') === 'dev') {
        app.set('trust proxy', 'loopback');
        app.set('env', 'development');
    }

    var storeCtx = new _auth0ExtensionTools2.default.WebtaskStorageContext(storage);
    var db = new _auth0ExtensionTools2.default.BlobRecordProvider(storeCtx);

    var extensibility = getExtensibility(config, db);

    // Maybe improve this check? 
    if (!extensibility.serializeUser) {
        extensibility = {
            strategy: extensibility
        };
    }

    extensibility.serializeUser = extensibility.serializeUser || function (user, done) {
        return done(null, user);
    };
    extensibility.deserializeUser = extensibility.deserializeUser || function (user, done) {
        return done(null, user);
    };

    if (extensibility.loginRouteHandler) {
        app.set('loginHandler', loginHandler);
    }

    app.use(_bodyParser2.default.json());
    app.use(_bodyParser2.default.urlencoded({
        extended: false
    }));

    app.use((0, _cookieParser2.default)());

    app.use((0, _expressSession2.default)({
        resave: true,
        saveUninitialized: true,
        secret: config('SESSION_SECRET')
    }));

    app.use((0, _connectFlash2.default)());

    app.use(_passport2.default.initialize());
    app.use(_passport2.default.session());

    _passport2.default.use('dynamicstrategy', extensibility.strategy);
    _passport2.default.serializeUser(extensibility.serializeUser);
    _passport2.default.deserializeUser(extensibility.deserializeUser);

    app.use((0, _morgan2.default)('dev'));
    app.use(_wsfedAsync2.default.createOptionsProvider(config, db));

    app.use('/', _wsfed2.default);

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handler
    app.use(function (err, req, res, next) {
        // render the error page
        console.log(err);
        res.status(err.status || 500);
        res.json(err);
    });

    return app;
}