'use strict';

require('babel-polyfill');

var _auth0ExtensionExpressTools = require('auth0-extension-express-tools');

var _auth0ExtensionExpressTools2 = _interopRequireDefault(_auth0ExtensionExpressTools);

var _setupExpress = require('./setupExpress');

var _setupExpress2 = _interopRequireDefault(_setupExpress);

var _UnauthorizedError = require('./UnauthorizedError');

var _UnauthorizedError2 = _interopRequireDefault(_UnauthorizedError);

var _auth0ExtensionTools = require('auth0-extension-tools');

var _auth0ExtensionTools2 = _interopRequireDefault(_auth0ExtensionTools);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

module.exports = function (options, cb) {
    options.nodejsCompiler(options.script, function () {
        var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(error, getExtensibility) {
            var expressFunc;
            return regeneratorRuntime.wrap(function _callee$(_context) {
                while (1) {
                    switch (_context.prev = _context.next) {
                        case 0:
                            // Expose global UnauthorizedError for strategies to use.
                            global.UnauthorizedError = _UnauthorizedError2.default;

                            if (!error) {
                                _context.next = 3;
                                break;
                            }

                            return _context.abrupt('return', cb(error));

                        case 3:
                            expressFunc = _auth0ExtensionExpressTools2.default.createServer(function (config, storage) {
                                return (0, _setupExpress2.default)(config, storage, getExtensibility);
                            });


                            cb(null, expressFunc);

                        case 5:
                        case 'end':
                            return _context.stop();
                    }
                }
            }, _callee, this);
        }));

        return function (_x, _x2) {
            return _ref.apply(this, arguments);
        };
    }());
};