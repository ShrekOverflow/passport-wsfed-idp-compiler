'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _passportMiddleware = require('../passportMiddleware');

var _passportMiddleware2 = _interopRequireDefault(_passportMiddleware);

var _UnauthorizedError = require('../UnauthorizedError');

var _UnauthorizedError2 = _interopRequireDefault(_UnauthorizedError);

var _wsfedAsync = require('../wsfedAsync');

var _wsfedAsync2 = _interopRequireDefault(_wsfedAsync);

var _ensureLoggedIn = require('../ensureLoggedIn');

var _ensureLoggedIn2 = _interopRequireDefault(_ensureLoggedIn);

var _userMiddleware = require('../userMiddleware');

var _userMiddleware2 = _interopRequireDefault(_userMiddleware);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = (0, _express.Router)();

router.use(_userMiddleware2.default);
router.get('/FederationMetadata/2007-06/FederationMetadata.xml', _wsfedAsync2.default.metadata);
router.use('/login', _passportMiddleware2.default);
router.get('/', (0, _ensureLoggedIn2.default)('login'), _wsfedAsync2.default.auth);
router.use(_wsfedAsync2.default.sendError);

exports.default = router;