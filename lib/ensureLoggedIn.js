'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ensureLoggedIn;

var _url = require('url');

var _url2 = _interopRequireDefault(_url);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function fixUrl(uri) {
    // strip search params
    uri = _url2.default.parse(uri).pathname;

    // If the URL ends with url do nothing
    if (uri.lastIndexOf('/') === uri.length - 1) {
        return uri;
    }

    return uri += '/';
} // A function to check if the user exists or not
// since we are not using passports inbuilt 
// req.logIn and req.logout yet we are just 
// setting the user in the flash hence, we 
// should use req.user instead of req.isAuthentication
// @TODO: Find out if we can use passport somehow 
// and use relative urls
function ensureLoggedIn(options) {

    if (typeof options == 'string') {
        options = {
            redirectTo: options
        };
    }

    options = options || {};

    var newPath = options.redirectTo || '/login';
    var setReturnTo = options.setReturnTo === undefined ? true : options.setReturnTo;

    return function ensureLoggedInRequestHandler(req, res, next) {
        if (!req.user) {
            if (setReturnTo && req.session) {
                req.session.returnTo = req.originalUrl;
            }

            // The `/` allows the url at the very end to be treated as a directory
            var currentUrl = fixUrl(req.originalUrl);
            var newUrl = _url2.default.resolve(currentUrl, newPath);
            return res.redirect(newUrl);
        }
        next();
    };
}