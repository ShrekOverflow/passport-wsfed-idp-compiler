'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = userMiddleware;
function userMiddleware(req, res, next) {
    req.user = req.flash('user')[0];
    req.set = function (user) {
        req.flash('user', user);
    };
    next();
}