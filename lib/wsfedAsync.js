'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _wsfed = require('wsfed');

var _wsfed2 = _interopRequireDefault(_wsfed);

var _getKeyandCertificates = require('./getKeyandCertificates');

var _getKeyandCertificates2 = _interopRequireDefault(_getKeyandCertificates);

var _urlParse = require('url-parse');

var _urlParse2 = _interopRequireDefault(_urlParse);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

// Wraps the node-wsfed module for webtask
// and fixes inconsistencies and exposes middlewares
// that could be uses as is
var wsfedAsync = {
    // This method is to be called by 
    // the app bootstrap method 
    // it'll setup the options provider
    // which will then be used by the framework
    createOptionsProvider: function createOptionsProvider(config, database) {
        var _this = this;

        return function () {
            var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(req, res, next) {
                var keys, query;
                return regeneratorRuntime.wrap(function _callee$(_context) {
                    while (1) {
                        switch (_context.prev = _context.next) {
                            case 0:
                                if (wsfedAsync.options) {
                                    _context.next = 5;
                                    break;
                                }

                                _context.next = 3;
                                return (0, _getKeyandCertificates2.default)(config, database);

                            case 3:
                                keys = _context.sent;

                                wsfedAsync.options = {
                                    getPostURL: function getPostURL(wtrealm, wreply, req, callback) {
                                        // Fix arity problem, the getPostURL is called with different
                                        // arities in different methods by node-wsfed
                                        if (callback === undefined) {
                                            callback = wreply;
                                        }
                                        return callback(null, 'https://' + config('AUTH0_DOMAIN') + '/login/callback');
                                    },

                                    issuer: config('ISSUER_NAME'),
                                    key: keys.private,
                                    cert: keys.cert
                                };

                            case 5:
                                query = req.query.wctx ? req.query : (0, _urlParse2.default)(req.session.returnTo, true).query;


                                req.wsfedOptions = Object.assign({}, wsfedAsync.options, {
                                    audience: query.wtrealm,
                                    wctx: query.wctx
                                });

                                next();

                            case 8:
                            case 'end':
                                return _context.stop();
                        }
                    }
                }, _callee, _this);
            }));

            return function (_x, _x2, _x3) {
                return _ref.apply(this, arguments);
            };
        }();
    },


    auth: function auth(req, res, next) {
        return _wsfed2.default.auth(req.wsfedOptions)(req, res, next);
    },

    metadata: function metadata(req, res, next) {
        return _wsfed2.default.metadata(req.wsfedOptions)(req, res, next);
    },

    sendError: function sendError(error, req, res, next) {
        if (UnauthorizedError.isUnauthorizedError(error)) {
            return _wsfed2.default.sendError(Object.assign({}, req.wsfedOptions, {
                fault: error || new UnauthorizedError('login_failed', 'Login failed for unknown reason')
            }))(req, res, next);
        }
        next(error);
    }
};
exports.default = wsfedAsync;