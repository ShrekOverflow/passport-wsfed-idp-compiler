'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _passport = require('passport');

var _passport2 = _interopRequireDefault(_passport);

var _urlParse = require('url-parse');

var _urlParse2 = _interopRequireDefault(_urlParse);

var _UnauthorizedError = require('./UnauthorizedError');

var _UnauthorizedError2 = _interopRequireDefault(_UnauthorizedError);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (req, res, next) {

    var returnTo = req.session.returnTo;

    // Authentication must start at the wsfed endpoint
    // otherwise deny the authentication

    if (!returnTo) {
        return next(new _UnauthorizedError2.default('Invalid Login attempt'));
    }

    req.authParams = (0, _urlParse2.default)(returnTo, true).query;

    _passport2.default.authenticate('dynamicstrategy', function (err, user, info) {

        // Handle the authentication request and 
        // if it fails we'll return to the client
        // if no loginHandler is provided, if one
        // is provided, send the request to the
        // login handler, this allows fully custom 
        // login forms to exist.

        if (user) {
            req.flash('user', user);
            return res.redirect(req.session.returnTo);
        }

        if (err) {
            return next(err);
        }

        var loginRouteHandler = req.app.get('loginHandler');

        if (!loginHandler) {
            return next(new _UnauthorizedError2.default('login_failed', info.message));
        }

        req.info = info;
        loginRouteHandler(req, res, next);
    })(req, res);
};