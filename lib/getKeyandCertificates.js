'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _selfsigned = require('selfsigned');

var _selfsigned2 = _interopRequireDefault(_selfsigned);

var _auth0ExtensionTools = require('auth0-extension-tools');

var _pemJwk = require('pem-jwk');

var _bluebird = require('bluebird');

var _bluebird2 = _interopRequireDefault(_bluebird);

var _crypto = require('crypto');

var _crypto2 = _interopRequireDefault(_crypto);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

var ALG_CRYPT = 'aes-256-ctr';

// @TODO: Optionally encrypt and decrypt the keys with a secret that only the webtask has

_bluebird2.default.promisifyAll(_selfsigned2.default);

function encrypt(text, password) {
    var cipher = _crypto2.default.createCipher(ALG_CRYPT, password);
    var crypted = cipher.update(text, 'utf8', 'hex');
    crypted += cipher.final('hex');
    return crypted;
}

function decrypt(text, password) {
    var decipher = _crypto2.default.createDecipher(ALG_CRYPT, password);
    var dec = decipher.update(text, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}

exports.default = function () {
    var _ref = _asyncToGenerator(regeneratorRuntime.mark(function _callee(config, db) {
        var pems, KEY_SECRET;
        return regeneratorRuntime.wrap(function _callee$(_context) {
            while (1) {
                switch (_context.prev = _context.next) {
                    case 0:
                        pems = null;
                        KEY_SECRET = config('KEY_SECRET');
                        _context.prev = 2;
                        _context.next = 5;
                        return db.get('_keys', '_root');

                    case 5:
                        pems = _context.sent;

                        pems = JSON.parse(decrypt(pems.box, KEY_SECRET));
                        _context.next = 17;
                        break;

                    case 9:
                        _context.prev = 9;
                        _context.t0 = _context['catch'](2);

                        if (!(_context.t0 instanceof _auth0ExtensionTools.NotFoundError)) {
                            _context.next = 17;
                            break;
                        }

                        _context.next = 14;
                        return _selfsigned2.default.generateAsync([{
                            name: 'commonName',
                            value: 'API Server'
                        }, {
                            name: 'organizationName',
                            value: config('ISSUER_NAME')
                        }], {
                            keySize: 2048,
                            algorithm: 'sha256',
                            days: 30
                        });

                    case 14:
                        pems = _context.sent;
                        _context.next = 17;
                        return db.create('_keys', {
                            box: encrypt(JSON.stringify(pems), KEY_SECRET),
                            _id: '_root'
                        });

                    case 17:
                        return _context.abrupt('return', pems);

                    case 18:
                    case 'end':
                        return _context.stop();
                }
            }
        }, _callee, this, [[2, 9]]);
    }));

    function getKeyandCertificates(_x, _x2) {
        return _ref.apply(this, arguments);
    }

    return getKeyandCertificates;
}();