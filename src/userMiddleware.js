
export default function userMiddleware(req, res, next) {
    req.user = req.flash('user')[0];
    req.set = (user) => {
        req.flash('user', user);
    };
    next();
}