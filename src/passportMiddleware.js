import passport from 'passport';
import parse from 'url-parse';
import UnauthorizedError from './UnauthorizedError';

export default (req, res, next) => {

    const returnTo = req.session.returnTo;

    // Authentication must start at the wsfed endpoint
    // otherwise deny the authentication
    
    if (!returnTo) {
        return next(new UnauthorizedError('Invalid Login attempt'));
    }

    req.authParams = parse(returnTo, true).query;

    passport.authenticate('dynamicstrategy', function (err, user, info) {
        
        // Handle the authentication request and 
        // if it fails we'll return to the client
        // if no loginHandler is provided, if one
        // is provided, send the request to the
        // login handler, this allows fully custom 
        // login forms to exist.

        if (user) {
            req.flash('user', user);     
            return res.redirect(req.session.returnTo);
        }
        
        if (err) {
            return next (err);
        }

        
        const loginRouteHandler = req.app.get('loginHandler');
        
        if (!loginHandler) {
            return next(new UnauthorizedError('login_failed', info.message));
        }

        req.info = info;
        loginRouteHandler(req, res, next);

    })(req, res);
}