import selfsigned from 'selfsigned';
import { NotFoundError } from 'auth0-extension-tools';
import { pem2jwk as toJwk } from 'pem-jwk';
import Bluebird from 'bluebird';
import crypto from 'crypto';
const ALG_CRYPT = 'aes-256-ctr';

// @TODO: Optionally encrypt and decrypt the keys with a secret that only the webtask has

Bluebird.promisifyAll(selfsigned);


function encrypt(text, password){
  var cipher = crypto.createCipher(ALG_CRYPT, password)
  var crypted = cipher.update(text, 'utf8', 'hex')
  crypted += cipher.final('hex');
  return crypted;
}

function decrypt(text, password) {
  var decipher = crypto.createDecipher(ALG_CRYPT, password)
  var dec = decipher.update(text, 'hex', 'utf8')
  dec += decipher.final('utf8');
  return dec;
}

export default async function getKeyandCertificates(config, db) {
    let pems = null;
    const KEY_SECRET = config('KEY_SECRET');

    try {
        // Get it from the database
        pems = await db.get('_keys', '_root');
        pems = JSON.parse(decrypt(pems.box, KEY_SECRET));
    } catch (e) {
        // Create and store them in database
        if (e instanceof NotFoundError) {
            pems = await selfsigned.generateAsync([{
                name: 'commonName',
                value: 'API Server'
            }, {
                name: 'organizationName',
                value: config('ISSUER_NAME')
            }], {
                    keySize: 2048,
                    algorithm: 'sha256',
                    days: 30,
                });

            await db.create('_keys', {
                box: encrypt(JSON.stringify(pems), KEY_SECRET),
                _id: '_root',
            });
        }
    }

    return pems;
}