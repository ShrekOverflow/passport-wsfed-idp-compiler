import wsfed from 'wsfed';
import getKeyandCertificates from './getKeyandCertificates';
import parse from 'url-parse';
// Wraps the node-wsfed module for webtask
// and fixes inconsistencies and exposes middlewares
// that could be uses as is
const wsfedAsync = {
    // This method is to be called by 
    // the app bootstrap method 
    // it'll setup the options provider
    // which will then be used by the framework
    createOptionsProvider(config, database) {
        return async (req, res, next) => {            
            if (!wsfedAsync.options) {
                const keys = await getKeyandCertificates(config, database);
                wsfedAsync.options = {
                    getPostURL(wtrealm, wreply, req, callback) {
                        // Fix arity problem, the getPostURL is called with different
                        // arities in different methods by node-wsfed
                        if (callback === undefined) {
                            callback = wreply;
                        }
                        return callback(null, `https://${config('AUTH0_DOMAIN')}/login/callback`);
                    },
                    issuer: config('ISSUER_NAME'),
                    key: keys.private,
                    cert: keys.cert,
                };
            }

            const query = req.query.wctx 
                ? req.query 
                : parse(req.session.returnTo, true).query;
            
            req.wsfedOptions = Object.assign({}, wsfedAsync.options, {
                audience: query.wtrealm,
                wctx: query.wctx
            });

            next();
        }
    },

    auth: (req, res, next) => wsfed.auth(req.wsfedOptions)(req, res, next),
    
    metadata: (req, res, next) => wsfed.metadata(req.wsfedOptions)(req, res, next),

    sendError(error, req, res, next) {
        if (UnauthorizedError.isUnauthorizedError(error)) {
            return wsfed.sendError(Object.assign({}, req.wsfedOptions, {
                fault: error || new UnauthorizedError('login_failed', 'Login failed for unknown reason'),
            }))(req, res, next);
        }
        next(error);
    }
};
export default wsfedAsync;
