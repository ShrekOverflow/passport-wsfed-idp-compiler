import express, { Router } from 'express';
import passportMiddleware from '../passportMiddleware';
import UnauthorizedError from '../UnauthorizedError';
import wsfed from '../wsfedAsync';
import ensureLoggedIn from '../ensureLoggedIn';
import userMiddleware from '../userMiddleware';
const router = Router();

router.use(userMiddleware);
router.get('/FederationMetadata/2007-06/FederationMetadata.xml', wsfed.metadata);
router.use('/login', passportMiddleware);
router.get('/', ensureLoggedIn('login'), wsfed.auth);
router.use(wsfed.sendError);

export default router;