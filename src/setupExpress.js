import tools from 'auth0-extension-tools';
import express from 'express';
import logger from 'morgan';
import session from 'express-session';
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import flash from 'connect-flash';
import passport from 'passport';
import wsfedRouter from './routes/wsfed';
import wsfedAsync from './wsfedAsync';

export default function setupExpress(config, storage, getExtensibility) {
    const app = express();

    // Allows this to sit-behind webtask and ngrok
    if (config('NODE_ENV') === 'dev') {
        app.set('trust proxy', 'loopback');
        app.set('env', 'development');
    }

    const storeCtx = new tools.WebtaskStorageContext(storage);
    const db = new tools.BlobRecordProvider(storeCtx);

    let extensibility = getExtensibility(config, db);

    // Maybe improve this check? 
    if (!extensibility.serializeUser) {
        extensibility = {
            strategy: extensibility
        }
    }

    extensibility.serializeUser = extensibility.serializeUser || ((user, done) => done(null, user));
    extensibility.deserializeUser = extensibility.deserializeUser || ((user, done) => done(null, user));

    if (extensibility.loginRouteHandler) {
        app.set('loginHandler', loginHandler);
    }

    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({
        extended: false
    }));

    app.use(cookieParser());

    app.use(session({
        resave: true,
        saveUninitialized: true,
        secret: config('SESSION_SECRET')
    }));

    app.use(flash());

    app.use(passport.initialize());
    app.use(passport.session());
    
    passport.use('dynamicstrategy', extensibility.strategy);
    passport.serializeUser(extensibility.serializeUser);
    passport.deserializeUser(extensibility.deserializeUser);


    app.use(logger('dev'));
    app.use(wsfedAsync.createOptionsProvider(config, db));

    app.use('/', wsfedRouter);

    // catch 404 and forward to error handler
    app.use(function (req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handler
    app.use(function (err, req, res, next) {
        // render the error page
        console.log(err);
        res.status(err.status || 500);
        res.json(err);
    });

    return app;
}