// A function to check if the user exists or not
// since we are not using passports inbuilt 
// req.logIn and req.logout yet we are just 
// setting the user in the flash hence, we 
// should use req.user instead of req.isAuthentication
// @TODO: Find out if we can use passport somehow 
// and use relative urls
import url from 'url';

function fixUrl(uri) {
    // strip search params
    uri = url.parse(uri).pathname;

    // If the URL ends with url do nothing
    if(uri.lastIndexOf('/') === (uri.length - 1)) {
        return uri;
    }

    return uri += '/';
}

export default function ensureLoggedIn(options) {

    if (typeof options == 'string') {
        options = {
            redirectTo: options 
        };
    }

    options = options || {};

    
    const newPath = options.redirectTo || '/login';
    const setReturnTo = (options.setReturnTo === undefined) ? true : options.setReturnTo;
    
    return function ensureLoggedInRequestHandler(req, res, next) {
        if (!req.user) {
            if (setReturnTo && req.session) {
                req.session.returnTo = req.originalUrl;
            }

            // The `/` allows the url at the very end to be treated as a directory
            const currentUrl = fixUrl(req.originalUrl);
            const newUrl = url.resolve(currentUrl, newPath);
            return res.redirect(newUrl);
        }
        next();
    }
}