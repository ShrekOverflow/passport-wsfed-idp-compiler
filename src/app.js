import 'babel-polyfill';
import expressTools from 'auth0-extension-express-tools';
import setupExpress from './setupExpress';
import UnauthorizedError from './UnauthorizedError';
import tools from 'auth0-extension-tools';

module.exports = function (options, cb) {
    options.nodejsCompiler(options.script, async function (error, getExtensibility) {
        // Expose global UnauthorizedError for strategies to use.
        global.UnauthorizedError = UnauthorizedError;

        if (error) {
            return cb(error);
        }

        const expressFunc = expressTools.createServer((config, storage) => {
            return setupExpress(config, storage, getExtensibility)
        });

        cb(null, expressFunc);
    });

}