// Test for steam openid

var SteamStrategy = require('passport-steam');

module.exports = function (config, database) {
    return new SteamStrategy({
        returnURL: config('STEAM_REALM') + '/login',
        realm: config('STEAM_REALM'),
        apiKey: config('STEAM_API_KEY')
    },function (identifier, profile, done) {
        profile.id = identifier;
        return done(null, profile);
    });
}