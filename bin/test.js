var CustomStrategy = require('passport-custom');

module.exports = function () {
    return new CustomStrategy(function (req, done) {
        if (req.authParams.login_hint === 'hakuna') {
            // For https://github.com/auth0/node-wsfed/blob/master/lib/claims/PassportProfileMapper.js#L38
            return done(null, {
                id: 'timonabcd',
                displayName: 'Timon Meerkat',
                givenName: 'Timon',
                familyName: 'Meerkat',
                emails: [{
                    value: 'timonmatata@savannah.jg'
                }]
            });
        }
        
        done(new UnauthorizedError('not_timon', 'You are not timon, no hakunamatata for you'));
    });
}